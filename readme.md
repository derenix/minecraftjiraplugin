What the heck is this?
======================

Hi there! :-) You're now looking at the source code for the [JIRA](http://www.atlassian.com/jira/) Plugin for [Minecraft](http://minecraft.net/).


**Minecraft** is an open-world sandbox game in which players mine, craft, explore and build an unlimited world made from blocks of different materials.


**JIRA** is a software application from [Atlassian](http://www.atlassian.com/) that lets developers, managers and other team members track their tasks and issues.


Using this plugin when playing Minecraft allows you to keep track of a 'to-do' list in Minecraft by creating and resolving JIRA issues. Win!


Getting Started
===============

You can download the latest version of the Minecraft JIRA Plugin by going to the [Downloads](/jaysee00/minecraftjiraplugin/downloads/) section. If you've never setup a Minecraft and/or JIRA Server before, there's also instructions for that in the [Wiki](/jaysee00/minecraftjiraplugin/wiki/).

Contributing
============

Please feel free to fork and update this repository! If you'd like to contact me, please use [twitter](https://twitter.com/jaysee00/).

Happy crafting!

